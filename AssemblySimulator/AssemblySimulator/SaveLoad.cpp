#include "SaveLoad.h"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <iterator>

SaveLoad::SaveLoad() {

}

int SaveLoad::save(string saveFileName, vector<commandLine> Program)
{
	string line;
	ofstream myfile(saveFileName);

	if (myfile.is_open()){
		for (int i = 0; i < Program.size(); i++){
			if (commandToString(Program.at(i).Command) == "ERROR") {
				return -1;
			}
			myfile << commandToString(Program.at(i).Command) << endl;
			myfile << intToString(int(Program.at(i).Value)) << endl;
			myfile << regToString(Program.at(i).Location1) << endl;
			myfile << regToString(Program.at(i).Location2) << endl;
		}
	}
	myfile.close();

	return 0;
}

vector<commandLine> SaveLoad::load(string loadFileName)
{

	vector<commandLine> Program;
	string command; string value; string reg1; string reg2;
	commandLine comandLineTemp;
	string line;

	ifstream myfile(loadFileName);
	if (myfile.is_open()){
		while (!(myfile.eof())){
			myfile >> command;
			myfile >> value;
			myfile >> reg1;
			myfile >> reg2;

			comandLineTemp.Command = stringToCommand(command);
			comandLineTemp.Value = stringToChar(value);
			comandLineTemp.Location1 = stringToRegister(reg1);
			comandLineTemp.Location2 = stringToRegister(reg2);
			Program.push_back(comandLineTemp);
		}
	}
	myfile.close();

	return Program;
}

Command SaveLoad::stringToCommand(string command)
{
	for (int i = 0; i < sizeof(CommandStrings); i++) {
		if (CommandStrings[i] == command){
			return Command(i);
		}
	}
	return NUL;
}
char SaveLoad::stringToChar(string value)
{
	return atoi(value.c_str());
}
Register SaveLoad::stringToRegister(string reg)
{
	for (int i = 0; i < Register(LAST_AT_END_REG); i++) {
		if (RegStrings[i] == reg){
			return Register(i);
		}
	}
}
string SaveLoad::commandToString(Command command)
{
	const char* tmp = CommandStrings[command];
	string temp = charStarToStr(tmp);
	tmp = NULL;
	return temp;
}
string SaveLoad::intToString(int value)
{;
	return to_string(value);
}
string SaveLoad::regToString(Register reg)
{
	return RegStrings[reg];
}

string SaveLoad::ToStr(char c)
{
	return string(1, c);
}

string SaveLoad::charStarToStr(const char* c)
{
	return string(c);
}


char* SaveLoad::toChr(string s) {
	char* returnChar = &s[0u];
	return returnChar;
}