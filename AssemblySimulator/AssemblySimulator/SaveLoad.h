#pragma once
#ifndef __H_SAVELOAD__
#define __H_SAVELOAD__

/*
@Author: BenskiBoy
@Created: 15/4/2016
*/


#include <string>
#include <vector>
#include <type_traits>
#include "CommandEnum.h"

using namespace std;

class SaveLoad {
private:
	string saveFileName;
	string loadFileName;



public:
	SaveLoad();
	int save(string saveFileName, vector<commandLine> Program);
	vector<commandLine> load(string loadFileName);

	Command stringToCommand(string command);
	char stringToChar(string value);
	Register stringToRegister(string reg);

	string commandToString(Command command);
	string intToString(int value);
	string regToString(Register reg);

	string SaveLoad::ToStr(char c);
	string SaveLoad::charStarToStr(const char* c);
	char* SaveLoad::toChr(string s);
};
#endif 
/*
Command Command;
char Value;
Register Location1;
Register Location2;
*/