#pragma once
#ifndef __H_CPU__
#define __H_CPU__

/*
@Author: BenskiBoy
@Created: 15/4/2016
*/

#include <string>
#include <vector>
#include <type_traits>
#include "CommandEnum.h"

using namespace std;

class CPU {
private:
	signed char Memory [MEM_SIZE];

	//though only 10 registers may be used, will create 255 to avoid errors
	//Higher registers may not be visible in interpreter
	signed char Registers [REG_SIZE];

	signed char tempRegister;

public:

	CPU();
	
	void ADD(Register Reg1, Register Reg2);
	void AND(Register Reg1, Register Reg2);
	bool CMP(Register Reg1, Register Reg2);
	void DEC(Register Reg1);

	void GET(Register Reg1, Register Reg2); //Put value from memory pointed to by Reg2 into Reg1
	void GET(signed char Value, Register Reg1); //Get value (direct reference)
	
	void INC(Register Reg1);
	//void JMP(Register Reg1);
	void LD(signed char Value, Register Reg1); //Reg1 = Value
	void MOV(Register Reg1, Register Reg2); //Memory location pointed to by Reg2 = memory location pointed to by Reg1
	void NOT(Register Reg1);
	void OR(Register Reg1, Register Reg2);
	void SUB(Register Reg1, Register Reg2);

	void STR(Register Reg1, Register Reg2); //Store value in Reg1 signed charo posigned char in memory posigned chared to by Reg2
	void STR(signed char Value, Register Reg1); //Store value in Value signed charo posigned char in memory posigned chared to by Reg2 
	//void PNT();
	char getRegValue(signed char Reg1) { return Registers[Reg1]; }
	char getMemValue(signed char Value) { return Memory[Value]; }
};
#endif 