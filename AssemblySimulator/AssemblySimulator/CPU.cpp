#include "CPU.h"

/*
@Author: BenskiBoy
@Created: 15/4/2016
*/

CPU::CPU()
{
	for (int i = 0; i < REG_SIZE; i++) {
		Registers[i] = 0;
	}
	for (int i = 0; i < MEM_SIZE; i++) {
		Memory[i] = 0;
	}
}

void CPU::ADD(Register Reg1, Register Reg2)
{
	Registers[Reg1] += Registers[Reg2];
}

void CPU::AND(Register Reg1, Register Reg2)
{
	Registers[Reg1] &= Registers[Reg2];
}

bool CPU::CMP(Register Reg1, Register Reg2)
{
	if (Registers[Reg1] == Registers[Reg2])
		return true;
	else
		return false;
}
void CPU::DEC(Register Reg1)
{
	if(Registers[Reg1] != 0)
		Registers[Reg1]--;
}

void CPU::GET(Register Reg1, Register Reg2) 
{
	Registers[Reg1] = Memory[Registers[Reg2]];
}
void CPU::GET(signed char Value, Register Reg1) 
{
	Registers[Reg1] = Memory[Registers[Value]];
}
void CPU::INC(Register Reg1) 
{
	Registers[Reg1]++;
}

void CPU::LD(signed char Value, Register Reg1)
{
	Registers[Reg1] = Value;
}

void CPU::MOV(Register Reg1, Register Reg2)
{
	Memory[Registers[Reg2]] = Memory[Registers[Reg1]];
}
void CPU::NOT(Register Reg1) 
{
	tempRegister = ~Registers[Reg1];
	Registers[Reg1] = tempRegister;
}
void CPU::OR(Register Reg1, Register Reg2) 
{
	Registers[Reg1] |= Registers[Reg2];
}

void CPU::SUB(Register Reg1, Register Reg2) 
{
	if(Registers[Reg1] - Registers[Reg2] > 0)
		Registers[Reg1] -= Registers[Reg2];
}
//Store value in Reg1 signed charo posigned char in memory posigned chared to by Reg2
void CPU::STR(Register Reg1, Register Reg2) 
{
	Memory[Registers[Reg2]] = Registers[Reg1];
}	

//Store value in Value signed charo posigned char in memory posigned chared to by Reg1 
void CPU::STR(signed char Value, Register Reg1)
{
	Memory[Registers[Reg1]] = Value;
}