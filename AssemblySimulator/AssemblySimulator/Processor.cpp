#include "Processor.h"

using namespace std;

/*
@Author: BenskiBoy
@Created: 15/4/2016
*/

Processor::Processor(vector<commandLine> Program)
{
	this->Program = Program;
	CPU cpu();
}

signed char Processor::execute()
{
	commandLine currentCommand;
	for (signed char i = 0; i < Program.size(); i++)
	{
		currentCommand = Program.at(i);
		switch (Program.at(i).Command) {
		case(ADD) :
			cpu.ADD(currentCommand.Location1, currentCommand.Location2);
			break;
		case(AND) :
			cpu.AND(currentCommand.Location1, currentCommand.Location2);
			break;
		case(CMP) :
			if (cpu.CMP(currentCommand.Location1, currentCommand.Location2) == true)
				i++;
			break;
		case(DEC) :
			cpu.DEC(currentCommand.Location1);
			break;
		case(GET) :
			if (currentCommand.Location1 != null)
				cpu.GET(currentCommand.Location1, currentCommand.Location2);
			else
				cpu.GET(currentCommand.Value, currentCommand.Location1);
			break;
		case(INC) :
			cpu.INC(currentCommand.Location1);
			break;
		case(JMP) :
			//Jump to posigned char in program to posigned chared by value
			if (currentCommand.Location1 == null)
				i = currentCommand.Value;
			//Jump to line in program to posigned chared by value register
			else
				i = cpu.getRegValue(currentCommand.Location1);
			break;
		case(LD) :
			cpu.LD(currentCommand.Value, currentCommand.Location1);
			break;
		case(MOV) :
			cpu.MOV(currentCommand.Location1, currentCommand.Location2);
			break;
		case(NOT) :
			cpu.NOT(currentCommand.Location1);
			break;
		case(OR) :
			cpu.OR(currentCommand.Location1, currentCommand.Location2);
			break;
		case(SUB) :
			cpu.SUB(currentCommand.Location1, currentCommand.Location2);
			break;
		case(PNT) :
			if(currentCommand.Location1 == null)
				cout << currentCommand.Value;
			else
				cout << cpu.getRegValue(currentCommand.Location1);
			break;
		case(STR) :
			if (currentCommand.Location2 == null)
				cpu.STR(currentCommand.Value, currentCommand.Location1);
			else
				cpu.STR(currentCommand.Location1, currentCommand.Location2);
			break;
		default:
			break;
		}

		dump(i);
	}

	return 0;
}

void Processor::dump(int commandNumber)
{
	int val;
	cout << endl << "SYSTEM DUMP at line: " << commandNumber << endl;
	cout << CommandStrings[Program.at(commandNumber).Command] << " " << int(Program.at(commandNumber).Value) << " " << RegStrings[Program.at(commandNumber).Location1] << " " << RegStrings[Program.at(commandNumber).Location2] << endl << "Memory" << endl;
	for (int i = 0; i < 128; i++) {
		cout << int(cpu.getMemValue(i)) << " ";
		if (i % 8 == 0 && i != 0)
			cout << endl;
	}

	cout << endl << endl << "Registers" << endl;
	for (int i = 0; i < 9; i++) {
		cout << int(cpu.getRegValue(i)) << " ";
	}

	cout << endl;
}