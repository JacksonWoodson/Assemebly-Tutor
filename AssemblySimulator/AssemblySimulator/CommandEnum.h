#pragma once
#ifndef __H_COMMANDENUM__
#define __H_COMMANDENUM__

/*
@Author: BenskiBoy
@Created: 15/4/2016
*/

#define REG_SIZE sizeof(char)*255
#define MEM_SIZE sizeof(char)*255

enum Command {
	NUL,
	ADD,	//Add
	AND,	//AND 
	CMP,	//Compare
	DEC,	//Decrement
	GET,	//Get (from memory)
	INC,	//Increment
	JMP,	//Jump
	LD,		//Load
	MOV,	//Move
	NOT,	//NOT
	OR,		//OR
	SUB,	//Subtract
	PNT,		//Prsigned char
	STR,	//Store (in memory)
	LAST_AT_END_COM //Used for knowing how many commands exist
};

static const char * CommandStrings[] = {
	"NUL",
	"ADD", 
	"AND", 
	"CMP",
	"DEC",
	"GET",
	"INC",
	"JMP",
	"LD",
	"MOV",
	"NOT",
	"OR",
	"SUB",
	"PNT",
	"STR",
	"LAST_AT_END_COM"
};

enum Register {
	R0 = 0,
	R1 = 1,
	R2 = 2,
	R3 = 3,
	R4 = 4,
	R5 = 5,
	R6 = 6,
	R7 = 7,
	null = 8,
	LAST_AT_END_REG = 9
};

static const char * RegStrings[] = {
	"Reg0",
	"Reg1",
	"Reg2",
	"Reg3",
	"Reg4",
	"Reg5",
	"Reg6",
	"Reg7",
	"----",
	"LAST_AT_END_REG"

};

struct commandLine {
	Command Command;
	char Value;
	Register Location1;
	Register Location2;
};

#endif