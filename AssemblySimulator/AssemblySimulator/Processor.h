#pragma once
#ifndef __H_PROCESSOR__
#define __H_PROCESSOR__

/*
@Author: BenskiBoy
@Created: 15/4/2016
*/

#include <iostream>
#include <string>
#include <vector>
#include <type_traits>
#include "CommandEnum.h"
#include "CPU.h"


class Processor{
private:
	vector<commandLine> Program;
	CPU cpu;

public:
	Processor(vector<commandLine> Program);
	signed char execute();
	void dump(int commandNumber);

};

#endif