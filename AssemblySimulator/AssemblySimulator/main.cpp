#include <iostream>
#include <vector>
#include "CommandEnum.h"
#include "Processor.h"
#include "SaveLoad.h"
/*
@Author: BenskiBoy
@Created: 15/4/2016
*/

using namespace std;

vector<commandLine> Program;

int main()
{
	SaveLoad sv;
	
	//	Command Command; char Value; Register Location1; Register Location2;
	
	/*
	commandLine com1 = { LD, 38, R0, null };
	commandLine com2 = { STR, 15, R0, null };
	commandLine com3 = { GET, 0, R1, R0 };
	commandLine com4 = { AND, 0, R0, R1 };
	commandLine com5 = { PNT, 0, R0, null };
	commandLine com6 = { PNT, 0, R1, null };

	Program.push_back(com1);
	Program.push_back(com2);
	Program.push_back(com3);
	Program.push_back(com4);
	Program.push_back(com5);
	Program.push_back(com6);
	*/

	//sv.save("SaveFile.as", Program);
	Program = sv.load("SaveFile.as");

	Processor prc(Program);

	prc.execute();

	return 0;
}